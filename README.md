# Android Error Factory

## Setup and run

- Ensure java SDK 19 is installed `apt install openjdk-19-jdk`
- Install Android SDK (can be done by installing Android Studio)
- Ensure ANDROID_SDK_ROOT is set `export ANDROID_SDK_ROOT=/home/$(whoami)/Android/Sdk`

1. in `app/src/main/AndroidManifest.xml`, replace `your DSN here` with the DSN
2. Launch an emulator, or connect a device
3. `./gradlew installDebug` should build a debug APK and install it into your connected device

At the moment all it does is throw an exception on load; see `app/src/main/java/com/example/androiderrorfactory/MainActivity.java`. I picked the tabbed navigation template when I bootstrapped the app in Android Studio with hopes
that it could be leveraged to put different stuff in later.
